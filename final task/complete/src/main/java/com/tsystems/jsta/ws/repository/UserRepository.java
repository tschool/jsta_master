package com.tsystems.jsta.ws.repository;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.t_systems.jsta.bookshelf.User;

import javax.annotation.PostConstruct;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Bulov, T-Systems RUS
 */
@Component
public class UserRepository {

  private static Map<String, User> USERS = new HashMap<>();

  @PostConstruct
  private void initData(){
    User dummy = new User ();
    dummy.setName ("Dummy");
    dummy.setAge (0);
    USERS.put ("1", dummy);
  }

  public User getUserByName (String name){
    return USERS.values().stream ().filter (user->user.getName().equals (name)).findFirst().orElse(null);
  }

  public List<User> getAllUsers (){
    return new ArrayList<> (USERS.values());
  }

  public int addUser(User user){
    USERS.put (user.getName (), user);
    return 1;
  }
}

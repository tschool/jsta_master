package com.tsystems.jsta.ws.repository;

import org.springframework.stereotype.Component;
import ru.t_systems.jsta.bookshelf.Author;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Bulov, T-Systems RUS
 */
@Component
public class AuthorRepository {
  private static final List<Author> AUTHORS = new ArrayList<> ();

  @PostConstruct
  public void initData() {
    Author knuth = new Author ();
    knuth.setName ("Donald Knuth");// the art of computer programming
    knuth.setPseudoym ("");
    AUTHORS.add (knuth);

    Author marshak = new Author ();
    marshak.setName ("Samuil Marshak");
    marshak.setPseudoym ("");
    AUTHORS.add (marshak);

    Author gandapas = new Author ();
    gandapas.setName ("Radislav Gandapas");
    gandapas.setPseudoym ("Radik");
    AUTHORS.add (gandapas);
  }

  public Author getAuthorByName(String name){
    return AUTHORS.stream ().filter (author -> author.getName().contains(name)).findFirst().orElse(null);
  }

  public void addAuthor(Author author){
    //TODO: implement
  }
}

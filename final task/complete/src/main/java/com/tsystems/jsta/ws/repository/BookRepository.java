package com.tsystems.jsta.ws.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t_systems.jsta.bookshelf.Book;
import ru.t_systems.jsta.bookshelf.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repo for books.
 *
 * @author Andrey Bulov, T-Systems RUS
 */
@Component
public class BookRepository {
  /**
   * Book list.
   */
  private static final Map<String, Book> BOOKS = new HashMap<>();

  @Autowired
  AuthorRepository authorRepository;

  @Autowired
  UserRepository userRepository;

  @PostConstruct
  public void initData() {
    Book book1 = new Book ();
    book1.setIsbn ("09897895432");
    book1.setName ("The Art of Computer Programming");
    book1.setOwner (userRepository.getUserByName ("Dummy"));
    book1.setMinAge (18);
    book1.setAuthor (authorRepository.getAuthorByName ("Knuth"));
    BOOKS.put(book1.getIsbn(), book1);

    Book book2 = new Book ();
    book2.setIsbn ("12356789");
    book2.setName ("Java for Dummies");
    book2.setAuthor (authorRepository.getAuthorByName ("Knuth"));
    book2.setOwner (null);
    book2.setMinAge (3);
    BOOKS.put(book2.getIsbn(), book2);

    Book book3 = new Book ();
    book3.setIsbn ("1235678900");
    book3.setAuthor (authorRepository.getAuthorByName ("Marshak"));
    book3.setName ("Gde Obedal Vorobey");
    book3.setMinAge (0);
    BOOKS.put(book3.getIsbn(), book3);

    Book book4 = new Book ();
    book4.setIsbn ("12348909");
    book4.setAuthor (authorRepository.getAuthorByName ("Gandapas"));
    book4.setOwner (null);
    book4.setName ("Ready for presentation");
    book4.setMinAge (30);
    BOOKS.put(book4.getIsbn(), book4);
  }

  public List<Book> getAllBooks() {
    return new ArrayList(BOOKS.values());
  }

  public Book findBookByName(String name){
    return getAllBooks().stream ().filter (book -> book.getName().contains(name)).findFirst().orElse(null);

  }

  public Book findBookByISBN(String isbn){
    return getAllBooks().stream().filter (book -> book.getIsbn().equals(isbn)).findFirst().orElse(null);
  }

  public Book rentBook (String isbn, User user) {
    Book book = findBookByISBN (isbn);
    if (book.getOwner () == null) {
      book.setOwner (user);
      BOOKS.put (book.getIsbn(), book);
      return book;
    }
    return null;
  }

  public int returnBook(Book bookToReturn){
    Book book = findBookByISBN (bookToReturn.getIsbn ());
    if(book!=null){
      book.setOwner (null);
      BOOKS.put(book.getIsbn(), book);
      return 0;
    }
    return -1;
  }
}

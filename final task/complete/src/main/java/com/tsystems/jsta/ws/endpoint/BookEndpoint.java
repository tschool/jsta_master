package com.tsystems.jsta.ws.endpoint;

import com.tsystems.jsta.ws.repository.BookRepository;
import com.tsystems.jsta.ws.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.util.TagUtils;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t_systems.jsta.bookshelf.AddUserRequest;
import ru.t_systems.jsta.bookshelf.AddUserResponse;
import ru.t_systems.jsta.bookshelf.Book;
import ru.t_systems.jsta.bookshelf.GetAllBooksResponse;
import ru.t_systems.jsta.bookshelf.GetAllUsersResponse;
import ru.t_systems.jsta.bookshelf.GetBookRequest;
import ru.t_systems.jsta.bookshelf.GetBookResponse;
import ru.t_systems.jsta.bookshelf.GetUserRequest;
import ru.t_systems.jsta.bookshelf.GetUserResponse;
import ru.t_systems.jsta.bookshelf.RentBookRequest;
import ru.t_systems.jsta.bookshelf.RentBookResponse;
import ru.t_systems.jsta.bookshelf.ReturnBookRequest;
import ru.t_systems.jsta.bookshelf.ReturnBookResponse;
import ru.t_systems.jsta.bookshelf.User;

/**
 * Endpoint for Books service.
 *
 * @author Andrey Bulov, T-Systems RUS
 */
@Endpoint
public class BookEndpoint {

  private static final String NAMESPACE_URI = "http://t-systems.ru/jsta/bookshelf";

  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private UserRepository userRepository;

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "getAllBooksRequest")
  @ResponsePayload
  public GetAllBooksResponse getAllBooks (){
    GetAllBooksResponse response = new GetAllBooksResponse ();
    response.getBooks().addAll(bookRepository.getAllBooks());
    return response;
  }

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "getBookRequest")
  @ResponsePayload
  public GetBookResponse getBook (@RequestPayload GetBookRequest request) {
    Book book = null;
    if(StringUtils.isEmpty (request.getIsbn())){
      book = bookRepository.findBookByName (request.getName());
    } else {
      book = bookRepository.findBookByISBN (request.getIsbn());
    }
    GetBookResponse response = new GetBookResponse ();
    response.setBook (book);
    return response;
  }

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "rentBookRequest")
  @ResponsePayload
  public RentBookResponse rentBook (@RequestPayload RentBookRequest request) {
    Book book = bookRepository.findBookByISBN(request.getBook().getIsbn());
    User user = userRepository.getUserByName(request.getUser().getName());
    RentBookResponse response = new RentBookResponse ();
    if(book!=null && user!=null){
      bookRepository.rentBook (book.getIsbn (), user);
      response.setBook (book);
    }
    return response;
  }

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "returnBookRequest")
  @ResponsePayload
  public ReturnBookResponse returnBook (@RequestPayload ReturnBookRequest request) {
    ReturnBookResponse response = new ReturnBookResponse ();
    response.setCode(bookRepository.returnBook (request.getBook ()));
    return response;
  }

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "getAllUsersRequest")
  @ResponsePayload
  public GetAllUsersResponse getAllUsers () {
    GetAllUsersResponse response = new GetAllUsersResponse ();
    response.getUser().addAll(userRepository.getAllUsers ());
    return response;
  }

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "getUserRequest")
  @ResponsePayload
  public GetUserResponse getUser (@RequestPayload GetUserRequest request) {
    GetUserResponse response = new GetUserResponse ();
    response.setUser (userRepository.getUserByName (request.getName ()));
    return response;
  }

  @PayloadRoot (namespace = NAMESPACE_URI, localPart = "addUserRequest")
  @ResponsePayload
  public AddUserResponse addUser (@RequestPayload AddUserRequest request) {
    AddUserResponse response = new AddUserResponse ();
    response.setCode (userRepository.addUser (request.getUser()));
    return response;
  }
}
